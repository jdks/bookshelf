require 'book'

describe Book do
  it 'can be initialised from query params' do
    params = { title: 'A Brave New World', author: 'Aldous Huxley' }
    book = Book.new(params)
    expect( book.title ).to eql 'A Brave New World'
  end

  it 'has a default image' do
    params = { title: ' The Blank Slate', author: 'Steven Pinker' }
    book = Book.new(params)
    expect( book.image ).to eql 'generic.png'
  end
end
