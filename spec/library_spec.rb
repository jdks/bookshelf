require 'library'

describe "Library" do 
  subject do
    Library.new('bookshelf_testing')
  end

  before do
    subject.clear
  end

  it 'should find a book if the search matches a book in the library' do
    subject.add_book(title: 'Grape')
    
    result = subject.search(title: 'Grape')

    expect(result.map(&:title)).to include('Grape')
  end

  it 'should error when searching with empty string' do
    mysearch = ""
    expect{subject.search(title: mysearch)}.to raise_error(ArgumentError)
  end

  it 'should error when searching with whitespace only string' do
    mysearch = "  "
    expect{subject.search(title: mysearch)}.to raise_error(ArgumentError)
  end

  it 'should find books with a partial match' do
    titles = %w/Sociology Psychology History/
    titles.each { |title| subject.add_book(title: title) }
    mysearch = "ology"
    result = subject.search(title: mysearch)

    expect(result.map(&:title)).to eq(['Sociology','Psychology'])
  end

  describe '#clear' do
    it 'should remove all books' do
      titles = %w/Sociology Psychology History/
      titles.each { |title| subject.add_book(title: title) }

      subject.clear

      expect(subject.size).to eq(0)
    end
  end

  describe '#update_book' do
    it 'should update the books attributes' do
      subject.add_book(title: 'Grape')
      url = 'hello.jpg'

      subject.update_book(image: url)

      result = subject.search(title: 'Grape').first
      expect(result.image).to eq('hello.jpg')
    end
  end
end
