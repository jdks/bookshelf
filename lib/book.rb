require 'mongo_mapper'

class Book
  include MongoMapper::Document

  key :title, String
  key :author, String
  key :image, String, default: 'generic.png'
end
