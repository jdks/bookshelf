class Library
  def initialize(database = 'bookshelf')
    MongoMapper.connection = Mongo::Connection.new
    MongoMapper.database = database
  end

  def add_book(params)
    book = Book.new(params)
    book.save
  end

  def search(params)
    res = Book
    if params.size == 1 and params.values.first =~ /^\s*$/
      raise ArgumentError
    end
    params.each do |k,v|
      res = res.where(k.to_sym => /#{v}/)
    end
    res.all
  end

  def update_book(params)
    first_item = bookshelf.first
    first_item.set(params)
    first_item.reload
    first_item.save
  end

  def clear
    bookshelf.remove
  end

  def size
    bookshelf.count
  end

  def books
    bookshelf.all
  end

  private

  attr_reader :bookshelf

  def bookshelf
    Book
  end
end
