require 'sinatra/base'
require 'sinatra/reloader'
require './lib/library'
require './lib/book'

class BookshelfApp < Sinatra::Base

  configure do
    set :library, Library.new
    set :show_exceptions, true
    register Sinatra::Reloader
    dont_reload 'views/'
  end

  get '/' do 
    erb :index
  end

  post '/' do
    books = settings.library.search(params)
    erb :results, locals: { books: books }
  end

  get '/add' do
    erb :add
  end

  post '/add' do
    settings.library.add_book(params)
    "You've added " + params[:title]
  end

  run! if app_file == $0
end
