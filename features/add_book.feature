Feature: Add a book
  In order to expand the bookshelf
  As a maintainer of a bookshelf
  I want to add a book

  Scenario: Add new book to bookshelf
    Given a bookshelf with a few books
    When I add a new book
    Then the new book is on the bookshelf
