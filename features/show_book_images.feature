Feature: Show book images
  In order to quickly identify a book
  As a user of a bookshelf
  I want to show book images

  Scenario: Book has no image
    Given a bookshelf with one book
    And the book has no image
    When I search for a book
    Then shows the book
    And shows a default image

  Scenario: Book has an image
    Given a bookshelf with one book
    And the book has an image
    When I search for a book
    Then shows the book
    And shows the books cover image
