module DomainDriver

  def library
    @library ||= Library.new('bookshelf_testing')
  end

  def search_by_title(title)
    @search_results = library.search(title: title)
  end

  def search_by_author(author)
    @search_results = library.search(author: author)
  end

  def search_results 
    @search_results.map(&:title)
  end

  def add_book(params)
    library.add_book(params)
  end

  def update_book(params)
    library.update_book(params)
  end

  def reset_library
    library.clear
  end

  def all_books
    library.books.map(&:title)
  end

  def default_image_shown?
    library.books.first.image == 'generic.png'
  end

end

module WebUIDriver

  def library
    unless @library
      @library = Library.new('bookshelf_testing')
      Capybara.app.set :library, @library
    end
    @library
  end

  
  def search_by_title(title)
    visit '/'
    fill_in 'query', with: title
    click_button 'Search'
  end

  def add_book(params)
    visit '/add'
    fill_in 'title', with: params[:title]
    fill_in 'author', with: params[:author]
    click_button 'Add'
  end

  def update_book(params)
    library.update_book(params)
  end

  def search_results
    all('.result').map(&:text)
  end

  def search_by_author(author)
    visit '/'
    fill_in 'author', with: author
    click_button 'Search'
  end

  def reset_library
    library.clear
  end

  def all_books
    library.books.map(&:title)
  end

  def default_image_shown?
    first('.results img')['src'] == 'generic.png'
  end

end
