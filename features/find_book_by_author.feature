Feature: Find a book by author
  In order to find a book
  As a user of a bookshelf
  I want to search by author

  Scenario: No such book on empty bookshelf
    Given an empty bookshelf
    When I search for a book by author
    Then shows no results found

  Scenario: No such books by that author on bookshelf
    Given a bookshelf with one book by another author
    When I search for a book by author
    Then shows no results found

  Scenario: Found a book by author
    Given a bookshelf with one book by author
    When I search for a book by author
    Then shows the book

  Scenario: Found multiple books by author
    Given a bookshelf with a few books by author
    When I search for a book by author
    Then shows all books by author

