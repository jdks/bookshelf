Before { reset_library }

Given(/^an empty bookshelf$/) do
end

Then(/^shows no results found$/) do
  expect( search_results ).to be_empty
end

Given(/^a bookshelf with one book$/) do
  add_book title: 'Ruby Programming'
end

When(/^I search for a non existent book$/) do
  search_by_title 'Banana'
end

When(/^I search for a book$/) do
  search_by_title 'Ruby'
end

When(/^I add a new book$/) do
  add_book title: 'Gang of Four'
end

Then(/^shows the book$/) do
  expect( search_results ).to eq ['Ruby Programming']
end

Then(/^the new book is on the bookshelf/) do
  expect( all_books ).to include('Gang of Four')
end

Given(/^a bookshelf with a few books$/) do
  add_book title: 'Ruby Programming'
  add_book title: 'Java Programming'
  add_book title: 'Perl Programming'
  add_book title: 'C# Programming'
end

When(/^I search for a book by author$/) do
  search_by_author 'Bob Joe'
end

Given(/^a bookshelf with one book by author$/) do
  add_book title: 'Ruby Programming', author: 'Bob Joe'
  add_book title: 'A Book on Bees', author: 'Jim Bob'
end

Given(/^a bookshelf with a few books by author$/) do
  add_book title: 'A famous tale', author: 'Bob Joe'
  add_book title: 'Another famous tale', author: 'Bob Joe'
  add_book title: 'YAFT: Yes, it\'s him again', author: 'Bob Joe'
end

Then(/^shows all books by author$/) do
  results = ['A famous tale', 'Another famous tale', 'YAFT: Yes, it\'s him again']
  expect( search_results ).to eq results
end

Given(/^a bookshelf with one book by another author$/) do
  add_book title: 'Not a famous book', author: 'Bobby Jim'
end

Given(/^the book has no image$/) do
end

Then(/^shows a default image$/) do
  expect( default_image_shown? ).to be_true
end

Given(/^the book has an image$/) do
  update_book image: 'lala.jpg'
end

Then(/^shows the books cover image$/) do
  expect( default_image_shown? ).to be_false
end
